# Bienvenue sur AFPATICKET!
## Layouts
### En-tête
Nous utilisons la même en-tête qui pourra apparaître sous 3 variantes, en fonction du statut de l'utilisateur parcourant l'application:

- utilisateur hors ligne
- utilisateur **stagiaire** connecté
- utilisateur **formateur** connecté

Les 3 variantes sont disponibles dans le fichiers **header.html** à la racine du dossier.

### CSS

**.text-underline**

**.bg-blue**

*IN WORK*

## Librairies
### FontAwesome
FontAwesome permet d'intégrer rapidement des icônes dans toutes formes de projets: web ou desktop.
Tous les fichiers nécessaires au fonctionnement de cette librairies ce situe dans le répertoire **/assets/fontawesome/**.
### Comment l'utiliser
De manière générale, on suit la convention qui nous est donné sur la documentation en créant des tags `<i></i>` avec une `class=""` portant le nom de l'icone que l'on souhaite.

Un exemple vaut mieux que deux page de théorie:
``<i  class="far fa-user"></i>``
Si on découpe les choses on peut voir plusieurs choses:
- far: "*fa*" pour *fontawesome* et "*r*" pour regular, et oui font signifie police !
- fa-user: le nom de l'icone souhaité.

Vous pouvez trouver toutes les **icônes gratuites** sur [gallerie du site](https://fontawesome.com/icons?d=gallery&m=free) de la librairie.
Si vous cliquez sur une icône, un exemple de code HTML vous sera donné à chaque fois.